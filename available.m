function A = available(G)
 % available Returns a matrix of the number of available cells in the
 %           neighborhood. 
 %
 %     A = available(G)
 %
 % Returns a matrix of the number of available cells in the neighborhood at
 % the given location in the geography matrix. A cell is available if its 
 % value is not -1.

  [m, n] = size(G);
  A = zeros(size(G));
  m = m-2;
  n = n-2;
  for i = 2:m+1
      for j = 2:n+1
          a = -1; % number of available cells
          for y = max(i-1, 1):min(i+1, m+1)
              for x = max(j-1, 1):min(j+1, n+1)
                  if (G(y, x) ~= -2)
                      a = a+1;
                  end
              end
          end
          A(i, j) = a;
      end
  end

end

