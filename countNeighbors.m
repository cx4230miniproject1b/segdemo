function [N, N_1, N_2] = countNeighbors(X)
  [m, n] = size (X);
  I = 2:m-1;
  J = 2:n-1;
  
  % count number of neighbors at each house
  N = zeros (size (X)); % total neighbors
  N_1 = N; % +1 neighbors
  N_2 = N; % -1 neighbors
  X_1 = X > 0;
  X_2 = X < 0;
  X = abs(X);
  N(I, J) = N(I, J) + X(I-1, J-1) + X(I-1, J) + X(I-1, J+1);
  N(I, J) = N(I, J) + X(I,   J-1) + X(I,   J+1);
  N(I, J) = N(I, J) + X(I+1, J-1) + X(I+1, J) + X(I+1, J+1);
  N_1(I, J) = N_1(I, J) + X_1(I-1, J-1) + X_1(I-1, J) + X_1(I-1, J+1);
  N_1(I, J) = N_1(I, J) + X_1(I,   J-1) + X_1(I,   J+1);
  N_1(I, J) = N_1(I, J) + X_1(I+1, J-1) + X_1(I+1, J) + X_1(I+1, J+1);
  N_2(I, J) = N_2(I, J) + X_2(I-1, J-1) + X_2(I-1, J) + X_2(I-1, J+1);
  N_2(I, J) = N_2(I, J) + X_2(I,   J-1) + X_2(I,   J+1);
  N_2(I, J) = N_2(I, J) + X_2(I+1, J-1) + X_2(I+1, J) + X_2(I+1, J+1);
end

% eof
