function G = createGeography(m, n, pos, neg, c, r)
 % createGeography Returns a happiness-valued geography matrix.
 %
 %   G = createGeography(m, n, pos, neg, c, r)
 %
 % Returns an (m+2) x (n+2) rectilinear grid of cells, G, initialized
 % randomly as follows.
 %
 % Each cell G(i, j) has a value in the closed interval [-c, c], 
 % where c is a value in [0, 1]. The boundary cells -- D(1,:), D(m+2,:),
 % D(:,1), and D(:,n+2) -- are set to -2. Exactly pos cells will have a value
 % c, and then each cell outwards will have value c*(1-d/r), where d is the
 % distance from the nearest cell with value 1. Additionally, exactly neg
 % cells will have value -c, and then each cell outwards will have value
 % -c*(1-d/r).
 %
 % The values in each cell have value as follows:
 %    -2: uninhabitable
 %    k : adds k to occupant's happiness value
 
  % Populate the geography, initially with nonnegative values
  X = zeros (m+2, n+2); 
  N = X;
  for t = 1:pos
      y = randi([2, m+1]);
      x = randi([2, n+1]);
      % Populate the cells around the center
      for i = max(y-r, 2):min(y+r, m+1)
          for j = max(x-r, 2):min(x+r, n+1)
              d = max(abs(y-i),abs(x-j));
              val = max(X(i, j), 1-d/r);
              k = N(i, j) + 1;
              N(i, j) = k;
              if (k == 1)
                  X(i, j) = val;
              else
                  X(i, j) = (k-1)/k*X(i, j) + val/k;
              end
          end
      end
  end
  for t = 1:neg
      y = randi([2, m+1]);
      x = randi([2, n+1]);
      % Populate the cells around the center
      for i = max(y-r, 2):min(y+r, m+1)
          for j = max(x-r, 2):min(x+r, n+1)
              d = max(abs(y-i),abs(x-j));
              val = max(X(i, j), 1-d/r);
              k = N(i, j) + 1;
              N(i, j) = k;
              if (k == 1)
                  X(i, j) = -val;
              else
                  X(i, j) = (k-1)/k*X(i, j) - val/k;
              end
          end
      end
  end
  c = min(max(c, 0), 1);
  X = c.*X;
  % Populate the border will -2's.
  X([1,m+2], 1:n+2) = -2;
  X(2:m+1, [1,n+2]) = -2;
  
  G = X;
end

