function G = createRandomGeography(m, n, mean, stdev)
 % createRandomGeography Returns a random happiness-valued geography matrix.
 %
 %   G = createRandomGeography(m, n, mean, stdev)
 %
 % Returns an (m+2) x (n+2) rectilinear grid of cells, G, initialized
 % randomly as follows.
 %
 % Each cell G(i, j) has a value in the closed interval [-c, c], 
 % where c is a value in [0, 1]. The boundary cells -- D(1,:), D(m+2,:),
 % D(:,1), and D(:,n+2) -- are set to -2. The values in each cell are
 % random numbers from a normal distribution with mean and standard
 % deviation as provided. Values exceeding c will be capped at c.
 %
 % The values in each cell have value as follows:
 %    -2: uninhabitable
 %    k : adds k to occupant's happiness value
 
  X = min(max(randn(m+2, n+2), -1), 1)*stdev + mean;
  % Populate the border will -2's.
  X([1,m+2], 1:n+2) = -2;
  X(2:m+1, [1,n+2]) = -2;
  
  G = X;
end

%eof