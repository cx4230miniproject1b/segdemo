function Y = moveGroupLocal (X, G, F, c)
% moveGroup  Randomly moves a subset of matrix elements.
%
%   Y = moveGroup (X, G, F, c)
%
% Given:
%   X(1:m, 1:n) = a matrix of {-1, 0, +1} values
%   G(1:m, 1:n) = a mask of values to try to move
%   F(1:m, 1:n) = a mask of potential destinations
%   c           = maximum distance to move
%
% For each G(i,j) == 1, this routine tries to move the value X(i,j) to
% some position (k, l) such that F(k,l) == 1. The position (k, l) is
% chosen uniformly at random from among all such positions as given by
% F. Returns a new matrix Y(1:m, 1:n) with both the "unmoved" and
% "moved" values.
  
  [m, n] = size (X);
  assert (all (size (F) == [m n]))

  % Determine who is movable
  [I_movable, J_movable, G_movable] = find (G);
  n_movable = length (I_movable);
  K_movable = randperm (n_movable);
  
  [I_free, J_free] = find (F);
  n_free = length (I_free); % no. of free locations
  K_free = randperm (n_free);

  % Determine who is moving and to where
  n_moving = min ([n_movable n_free]);
  K_moving = K_movable(1:n_moving);
  Y = X;
  for k1 = 1:n_moving
      k_free = [];
      k = K_movable(k1);
      for k2 = 1:size(K_free)
          kf = K_free(k2);
         I_movable(k) - c <= I_free(kf);
         if I_movable(k) - c <= I_free(kf) && I_free(kf) <= I_movable(k) + c 
             if J_movable(k) - c <= J_free(kf) && J_free(kf) <= J_movable(k) + c
                k_free = [k_free kf];
             end
         end
      end
      if ~isempty(k_free)
          k_dest = randperm(k_free);
          k_dest = k_dest(1);
          move_outs = sparse(I_movable(k), J_movable(k), G_movable(k), m, n);
          move_ins = sparse(I_free(k_dest), J_free(k_dest), G_movable(k), m,n);
          Y = Y - move_outs + move_ins;
          K_free = K_free(K_free ~= k_dest);
      end
  end
end

% eof