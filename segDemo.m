%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%
SHOW_DETAIL = true;

% Domain dimensions
NUM_ROWS = 20;
NUM_COLS = NUM_ROWS; % square grid

% Population parameters
PROB_EMPTY = 0.3; % probability that a cell is empty
POP_RATIO = 1; % population bias: ratio of group "A" ("+1") size to "B" ("-1") size
POP_TOLERANCE = .5; % ratio of different to total neighbors that will be tolerated (happiness)

% Geography paramaters
GEO_ON = false; % should geography affect happiness
GEO_RANDOM_NORMAL = false; % should geography happiness values be assigned random values from a normal distribution
GEO_POS_CENTERS = 2; % number of maximum values for geography
GEO_NEG_CENTERS = 1; % number of minimum values for geography
GEO_HAPPINESS_SCALE = .5; % value in [0,1] to determine the maximum happiness offered by geography
GEO_RADIUS = 5; % effect radius of centers
GEO_MEAN = 0; % mean for normal distribution
GEO_STDEV = .25; % standard deviation for normal distribution

% Movement parameters
MOVE_LOCAL_ON = false; % toggles local movement
MOVE_DISTANCE = 3; % maximum distance a person can move

% Maximum number of time steps
MIN_DIM = min ([NUM_ROWS NUM_COLS]);
MAX_TIME = round (MIN_DIM * log (MIN_DIM));

%%%%%%%%%%%%%
% Main Demo %
%%%%%%%%%%%%%

% Initial grid
X_0 = createDomain (NUM_ROWS, NUM_COLS, PROB_EMPTY, POP_RATIO);
G = createGeography(NUM_ROWS, NUM_COLS, GEO_POS_CENTERS, ...
     GEO_NEG_CENTERS, GEO_HAPPINESS_SCALE, GEO_RADIUS);
if (GEO_ON)
    if (GEO_RANDOM_NORMAL)
        G = createRandomGeography(NUM_ROWS, NUM_COLS, GEO_MEAN, GEO_STDEV);
    end
end
    
% Draw initial grid
figure (1); clf;
n_A = length (find (X_0 > 0)); % # in group "A" ("+1")
n_B = length (find (X_0 < 0)); % # in group "B" ("-1")
N_A = n_A; % absolute # in group A
N_B = n_B; % absolute # in group B
[N_0, N_1_0, N_2_0] = countNeighbors(X_0);
[measure, S_0] = segMeasure(X_0, G, N_A, N_B, N_1_0, N_2_0);

subplot(2, 2, 1); showDomain(X_0, sprintf('Initial\n[%d:%d]', n_A, n_B));
subplot(2, 2, 2); showDomain(S_0, sprintf('Initial Segregation Measure\n%d', measure));
if (GEO_ON)
    if (GEO_RANDOM_NORMAL)
        subplot(2, 2, 3); showDomain(G, sprintf('Geography\nN(%d,%d)',GEO_MEAN, GEO_STDEV));
    else
        subplot(2, 2, 3); showDomain(G, sprintf('Geography\n[%d, %d, %d]', ... 
            GEO_POS_CENTERS, GEO_HAPPINESS_SCALE, GEO_RADIUS));
    end
end
pause

% Main simulation (time-stepping) loop
X_t = X_0;
for t = 1:MAX_TIME,

  display (sprintf ('=== Time step: %d ===', t))

  % Measure the "color" of each neighborhood
  C_t = measureColor (X_t);
  
  % Count number of neighbors
  [N_t, N_1_t, N_2_t] = countNeighbors(X_t);
  
  % Determine who is unhappy with their neighborhood
  %M_t = (X_t ~= 0) & (X_t .* C_t) <= 0);
  M_t = (X_t ~= 0) & ((((X_t > 1).*N_2_t + (X_t < 0).*N_1_t)./N_t - GEO_ON.*G) > POP_TOLERANCE);

  % Move the "A" ("+1") group first
  A_t = (X_t > 0); % Group "A" ("+1")
  F_a_t = getFree (X_t);
  F_A_t = F_a_t .* (C_t >= 0); % desirable locations
  if (sum(F_A_t(:)) < max(sum(A_t(:))/2, t))
      F_A_t = F_a_t;
  end
  if (MOVE_LOCAL_ON)
      Y_t = moveGroupLocal(X_t, A_t .* M_t, F_A_t, MOVE_DISTANCE);
  else
      Y_t = moveGroup(X_t, A_t .* M_t, F_A_t);
  end
      
  % Move the "B" ("-1") group next
  B_t = -(X_t < 0); % Group "B" ("-1")
  F_b_t = getFree (Y_t);
  F_B_t = F_b_t .* (C_t <= 0); % desirable locations
  if (sum(F_B_t(:)) < max(sum(B_t(:))/2, t))
      F_B_t = F_b_t;
  end
  if (MOVE_LOCAL_ON)
      Z_t = moveGroupLocal(Y_t, B_t .* M_t, F_B_t, MOVE_DISTANCE);
  else
      Z_t = moveGroup(Y_t, B_t .* M_t, F_B_t);
  end    
  
  % segregation measure
  [measure, S_t] = segMeasure(X_t, G, N_A, N_B, N_1_t, N_2_t);

  n_moved = sum (sum (abs (abs (Z_t) - abs (X_t)))) / 2;
  display (sprintf ('  %d cells were movable.', sum (sum (M_t))));
  display (sprintf ('  %d cells moved.', n_moved));
  display(sprintf('  Segregation measure is %d.', measure));

  figure (1); clf;
  if SHOW_DETAIL,
    subplot (2, 3, 1); showDomain (X_t, sprintf ('Population\nt=%d', t));
    n_A = length (find (C_t == 1)); n_B = length (find (C_t == -1));
    subplot (2, 3, 2); showDomain (C_t, sprintf ('Neighborhood color\n[%d:%d]', n_A, n_B));
    n_M = length (find (M_t ~= 0));
    subplot (2, 3, 3); showDomain (M_t, sprintf ('Movable\n[%d]', n_M));
    subplot (2, 3, 4); showDomain (Z_t, sprintf ('New population\nt+1=%d', t+1));
    subplot(2, 3, 5); showDomain(S_t, sprintf('Segregation Measure\n%d', measure));
    if (GEO_ON)
        if (GEO_RANDOM_NORMAL)
            subplot(2, 3, 6); showDomain(G, sprintf('Geography\nN(%d,%d)',GEO_MEAN, GEO_STDEV));
        else
            subplot(2, 3, 6); showDomain(G, sprintf('Geography\n[%d, %d, %d]', ... 
                GEO_POS_CENTERS, GEO_HAPPINESS_SCALE, GEO_RADIUS));
        end
    end
  else
    n_A = length (find (X_0 == 1)); n_B = length (find (X_0 == -1));
    subplot (1, 2, 1); showDomain (X_0, sprintf ('Initial\n[%d:%d]: t=0', n_A, n_B));
    n_A = length (find (Z_t == 1)); n_B = length (find (Z_t == -1));
    subplot (1, 2, 2); showDomain (Z_t, sprintf ('Current population\n[%d:%d]: t=%d', n_A, n_B, t));
  end

  pause;
  X_t = Z_t;

  if n_moved == 0,
    display ('*** END: Reached steady state. ***')
    break;
  end
end

% eof
