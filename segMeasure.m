function [measure, locals] = segMeasure(X, G, n_1, n_2, N_1_t, N_2_t)
 % segMeasure Returns a segregation measure for the given domain.
  [measure, locals] = segMeasure1(X, G, n_1, n_2, N_1_t, N_2_t);
end

function [measure, locals] = segMeasure1(X, G, n_1, n_2, N_1_t, N_2_t)
 % segMeasure1 Returns a segregation measure for the given domain.
 %
 %     [measure, locals] = segMeasure1(X, G, n_1, n_2)
 %
 % Returns a segregation measure, and a matrix with the local measures
 % according to the following formula.
 %
 %     local(i,j) = [(N_1-N_2)-(n_1(i, j)-n_2(i, j))/a]^2 if X(i, j) is
 %     occupied, 0 otherwise
 % 
 %
 % where N_k is the average distribution of the first and second
 % color respectively given by n_k/(m*n),
 % n_k(i, j) is the number of neighbors of color k in the neighborhood, and
 % a is the number of possible neighbors.
 % 
 % The total segregation measure is the local average of all non-empty cells.
 
  [m, n] = size(X);
  A = available(G);
  locals = zeros(size(X));
  m = m-2;
  n = n-2;
  pop = n_1 + n_2;
  N_1 = n_1/(m*n); % 1
  N_2 = n_2/(m*n); % -1
  measure = 0;
  for i = 2:m+1
      for j = 2:n+1
          % calculate neighborhood statistics
          if (X(i, j) ~= 0) 
              n_1 = N_1_t(i, j); % number of "1"
              n_2 = N_2_t(i, j); % number of "-1"
              val = [(N_1-N_2)-(n_1-n_2)/A(i, j)]^2;
              locals(i, j) = val;
              measure = measure + locals(i, j);
          end
      end
  end
  measure = measure/pop;
end